package com.bizmda.bizsip.app.config;

import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.config.SinkConfigMapping;
import com.bizmda.log.trace.RestTemplateTraceInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

/**
 * @author 史正烨
 */
@Slf4j
@Configuration
public class AppConfiguration {
    @Value("${bizsip.config-path:#{null}}")
    private String configPath;

    @Bean
    public AppServiceMapping appServiceMapping() throws BizException {
        return new AppServiceMapping(this.configPath);
    }

    @Bean
    public SinkConfigMapping sinkConfigMapping() throws BizException {
        return new SinkConfigMapping(this.configPath);
    }

    @Bean
    public CheckRuleConfigMapping checkRuleConfigMapping() throws BizException {
        return new CheckRuleConfigMapping(this.configPath);
    }

    @Bean
    public ControlRuleConfigMapping controlRuleConfigMapping() throws BizException {
        return new ControlRuleConfigMapping(this.configPath);
    }

    @Bean
    public ControlRuleMetricConfigMapping controlRuleMetricConfigMapping() throws BizException {
        return new ControlRuleMetricConfigMapping(this.configPath);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate()
    {
        RestTemplate restTemplate = new RestTemplate();
        log.info("RestTemplate调用增加日志跟踪能力拦截器");
        restTemplate.setInterceptors(Collections.singletonList(new RestTemplateTraceInterceptor()));
        return restTemplate;
    }
}
