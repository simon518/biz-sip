package com.bizmda.bizsip.app.controller;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.service.AppService;
import com.bizmda.bizsip.common.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author 史正烨
 */
@Slf4j
@RestController
//@RequestMapping("/")
public class AppController {
    @Autowired
    private AppService appService;

    @PostMapping(value="/api",consumes = "application/json", produces = "application/json")
    public BizMessage<JSONObject> doApiService(HttpServletRequest request, HttpServletResponse response,
                                               @RequestBody JSONObject inJsonObject,
                                               @PathVariable(required = false) Map<String, Object> pathVariables,
                                               @RequestParam(required = false) Map<String, Object> parameters) {
        String serviceId = request.getHeader("Biz-Service-Id");
        return appService.process(serviceId,inJsonObject);
    }
}
