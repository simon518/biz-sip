package com.bizmda.bizsip.app.listener;

import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.bizmda.bizsip.app.config.AppServiceMapping;
import com.bizmda.bizsip.app.config.RabbitmqConfig;
import com.bizmda.bizsip.app.executor.AbstractAppExecutor;
import com.bizmda.bizsip.app.service.AppService;
import com.bizmda.bizsip.common.*;
import com.bizmda.bizsip.service.AppLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * RabbitMQ接收服务
 */
@Slf4j
@Service
public class AppServiceQueueListener {
    @Autowired
    private AppService appService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    private Jackson2JsonMessageConverter jackson2JsonMessageConverter =new Jackson2JsonMessageConverter();

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = BizConstant.APP_SERVICE_QUEUE, durable = "true", autoDelete = "false"),
            exchange = @Exchange(value = BizConstant.APP_SERVICE_EXCHANGE, type = ExchangeTypes.DIRECT, durable = "true", autoDelete = "false"),
            key = BizConstant.APP_SERVICE_ROUTING_KEY))
    public void onMessage(Message inMessage) {
        try {
            this.process(inMessage);
        } catch (Exception e) {
            log.error("App服务侦听器处理出错!",e);
            return;
        }
    }

    private void process(Message inMessage) {
        JSONObject jsonObject = null;
        try {
            String str = new String(inMessage.getBody(), "UTF-8");
            jsonObject = JSONUtil.parseObj(str);
        } catch (UnsupportedEncodingException e) {
            log.error("不支持的编码!",e);
            return;
        }
        BizMessage<JSONObject> bizMessage = this.appService.process(jsonObject.getStr("app-service-id"),jsonObject.getJSONObject("data"));
        JSONObject jsonObject1 = JSONUtil.parseObj(bizMessage);
        String correlationId = inMessage.getMessageProperties().getCorrelationId();
        String replyTo = inMessage.getMessageProperties().getReplyTo();
        if (StrUtil.isEmpty(replyTo)) {
            return;
        }
        Message message = MessageBuilder.withBody(JSONUtil.toJsonStr(jsonObject1).getBytes(StandardCharsets.UTF_8))
                .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                .setContentType(MessageProperties.CONTENT_TYPE_JSON)
                .setCorrelationId(correlationId).build();
        this.rabbitTemplate.send("", replyTo, message);
        return;
    }
}
