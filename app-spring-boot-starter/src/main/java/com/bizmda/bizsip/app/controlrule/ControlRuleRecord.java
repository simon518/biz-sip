package com.bizmda.bizsip.app.controlrule;

import lombok.Builder;
import lombok.Data;
import java.io.Serializable;

@Data
public class ControlRuleRecord implements Serializable {
    private long time;
    private long amount;

    public ControlRuleRecord() {
    }
}
