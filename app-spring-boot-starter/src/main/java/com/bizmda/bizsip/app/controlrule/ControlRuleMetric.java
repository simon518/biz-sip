package com.bizmda.bizsip.app.controlrule;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
public class ControlRuleMetric implements Serializable {
    private long count = 0;
    private long amount = 0;

    public ControlRuleMetric() {
    }
}
