package com.bizmda.bizsip.app.executor.script;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.bizmda.bizsip.app.config.ControlRuleMetricConfigMapping;
import com.bizmda.bizsip.app.controlrule.ControlRuleMetric;
import com.bizmda.bizsip.app.service.AppClientService;
import com.bizmda.bizsip.common.*;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.config.RabbitmqSinkConfig;
import com.bizmda.bizsip.config.RestSinkConfig;
import com.bizmda.bizsip.config.SinkConfigMapping;
import com.open.capacity.redis.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.client.RestTemplate;
import org.ssssssss.magicapi.config.MagicModule;
import org.ssssssss.script.annotation.Comment;

import java.util.HashMap;
import java.util.Map;

import static com.bizmda.bizsip.app.service.AppClientService.PREFIX_SIP_ASYNCLOG;

/**
 * @author 史正烨
 */
@Slf4j
public class SipFunctions implements MagicModule {

    private static SinkConfigMapping sinkConfigMapping = null;
    private static RestTemplate restTemplate = null;
    private static AppClientService appClientService = null;
    private static RabbitTemplate rabbitTemplate = null;
    private static RedisUtil redisUtil = null;
    private static ControlRuleMetricConfigMapping controlRuleMetricConfigMapping = null;

    @Comment("执行适配器服务调用")
    public static BizMessage<JSONObject> callSink(@Comment("服务ID") String sinkId, @Comment("调用输入参数") Object inData) {
        log.trace("callSink({},{})", sinkId, inData);
        JSONObject jsonObject = JSONUtil.parseObj(inData);
        if (restTemplate == null) {
            restTemplate = SpringUtil.getBean(RestTemplate.class);
        }
        if (sinkConfigMapping == null) {
            sinkConfigMapping = SpringUtil.getBean(SinkConfigMapping.class);
        }

        BizMessage<JSONObject> inMessage = BizTools.bizMessageThreadLocal.get();
        inMessage.setData(jsonObject);

        AbstractSinkConfig sinkConfig = null;
        try {
            sinkConfig = (AbstractSinkConfig) sinkConfigMapping.getSinkConfig(sinkId);
        } catch (BizException e) {
            return BizMessage.buildFailMessage(inMessage,e);
        }
        BizMessage<JSONObject> outMessage = null;
        if (sinkConfig.getType() == AbstractSinkConfig.TYPE_REST) {
            RestSinkConfig restServerAdaptorConfig = (RestSinkConfig) sinkConfig;
            log.trace("调用Restful服务: {}", restServerAdaptorConfig.getUrl());
            log.trace("Rest请求报文:\n{}", BizUtils.buildBizMessageLog(inMessage));
            outMessage = restTemplate.postForObject(restServerAdaptorConfig.getUrl(), inMessage, BizMessage.class);
        } else if (sinkConfig.getType() == AbstractSinkConfig.TYPE_RABBITMQ) {
            RabbitmqSinkConfig rabbitmqSinkConfig = (RabbitmqSinkConfig) sinkConfig;
            log.trace("调用RabbitMQ服务: exchange[{}],route-key[{}]",
                    rabbitmqSinkConfig.getExchange(), rabbitmqSinkConfig.getRoutingKey());
            if (rabbitTemplate == null) {
                rabbitTemplate = SpringUtil.getBean(RabbitTemplate.class);
            }
            rabbitTemplate.convertAndSend(rabbitmqSinkConfig.getExchange(),
                    rabbitmqSinkConfig.getRoutingKey(), inMessage);
            return BizMessage.buildSuccessMessage(inMessage, new JSONObject());
        } else {
            log.error("未知的Sink类型:" + sinkConfig.getType());
            return null;
        }
        if (!(outMessage.getData() instanceof JSONObject)) {
            outMessage.setData(JSONUtil.parseObj(outMessage.getData()));
        }
//        log.debug("返回:\n{}", BizUtils.buildBizMessageLog(outMessage));
        return outMessage;
    }

    @Comment("执行SAF服务调用")
    public static BizMessage<JSONObject> doDelayService(@Comment("服务ID") String serviceId, @Comment("调用输入参数") Object inData, @Comment("延迟毫秒数") String millisecondStr) {
        log.trace("doDelayService({},{},{})", serviceId, inData, millisecondStr);
        if (appClientService == null) {
            appClientService = SpringUtil.getBean(AppClientService.class);
        }
        JSONObject jsonObject = JSONUtil.parseObj(inData);

        BizMessage<JSONObject> inMessage = BizTools.bizMessageThreadLocal.get();
        inMessage.setData(jsonObject);

        String[] millisecondStrArray = millisecondStr.split(",");
        int[] milliseconds = new int[millisecondStrArray.length];
        for (int i = 0; i < millisecondStrArray.length; i++) {
            milliseconds[i] = Integer.parseInt(millisecondStrArray[i]);
        }
        BizMessage<JSONObject> outMessage = appClientService.callDelayAppService(serviceId, inData, milliseconds);
        log.trace("返回:\n{}", BizUtils.buildBizMessageLog(outMessage));
        return outMessage;
    }

    @Comment("获取SAF服务的当前重试次数")
    public static int getServiceRetryCount() {
        log.trace("getServiceRetryCount()");
        TmContext tmContext = BizTools.tmContextThreadLocal.get();
        log.trace("返回:{}", tmContext.getRetryCount());
        return tmContext.getRetryCount();
    }

    @Comment("保存异步服务上下文")
    public void saveAsyncContext(@Comment("全局交易索引键") String transactionKey, @Comment("异步上下文变量") Object context, @Comment("异步服务超时时间") long timeout) {
        log.trace("saveAsyncContext({},{},{})", transactionKey, timeout, context);
        BizMessage bizMessage = BizTools.bizMessageThreadLocal.get();
        Map<String, Object> map = new HashMap<>(16);
        map.put("traceId", bizMessage.getTraceId());
        map.put("context", context);
        if (redisUtil == null) {
            redisUtil = SpringUtil.getBean(RedisUtil.class);
        }
        redisUtil.set(PREFIX_SIP_ASYNCLOG + transactionKey, context, timeout);
    }

    /**
     * 恢复异步服务上下文
     *
     * @param transactionKey 异步回调的全局唯一交易索引键
     * @return 异步服务上下文
     */
    @Comment("恢复异步服务上下文")
    public Object loadAsyncContext(@Comment("全局交易索引键") String transactionKey) {
        log.trace("loadAsyncContext({})", transactionKey);
        if (redisUtil == null) {
            redisUtil = SpringUtil.getBean(RedisUtil.class);
        }
        Map<String, Object> map = (Map<String, Object>) redisUtil.get(PREFIX_SIP_ASYNCLOG + transactionKey);
        if (map == null) {
            return null;
        }
        String traceId = (String) map.get("traceId");
        Object context = map.get("context");
        BizMessage bizMessage = BizTools.bizMessageThreadLocal.get();
        if (bizMessage.getParentTraceId() == null) {
            log.trace("重置parentTraceId:{}", traceId);
            bizMessage.setParentTraceId(traceId);
            BizTools.bizMessageThreadLocal.set(bizMessage);
            log.trace("返回:{}", context);
            return context;
        } else if (bizMessage.getParentTraceId().equals(traceId)) {
            log.trace("返回:{}", context);
            return context;
        } else {
            log.trace("返回:{}", context);
            return context;
        }
    }

    @Comment("构建返回错误信息")
    public static ExecutorError error(String message) {
        log.trace("error({})", message);
        ExecutorError error = new ExecutorError();
        error.setMessage(message);
        error.setTimeoutException(false);
        return error;
    }

    @Comment("构建返回错误信息")
    public static ExecutorError timeout() {
        log.trace("timeout()");
        ExecutorError error = new ExecutorError();
        error.setTimeoutException(true);
        error.setMessage("调用timeout()导致超时错误!");
        return error;
    }

    public static ControlRuleMetric getMetric(String name,String key) {
        if (controlRuleMetricConfigMapping == null) {
            controlRuleMetricConfigMapping = SpringUtil.getBean(ControlRuleMetricConfigMapping.class);
        }
        try {
            return controlRuleMetricConfigMapping.getMetric(name).getMetric(key);
        } catch (BizException e) {
            log.error("getMeteic({},{})",name,key,e);
            return null;
        }
    }

    public static void addRecord(String name,String key,long amount) {
        if (controlRuleMetricConfigMapping == null) {
            controlRuleMetricConfigMapping = SpringUtil.getBean(ControlRuleMetricConfigMapping.class);
        }
        try {
            controlRuleMetricConfigMapping.getMetric(name).addRecord(key,amount);
        } catch (BizException e) {
            log.error("addRecord({},{},{})",name,key,amount,e);
        }
    }

    public static void deleteRecord(String name,String key,long amount) {
        if (controlRuleMetricConfigMapping == null) {
            controlRuleMetricConfigMapping = SpringUtil.getBean(ControlRuleMetricConfigMapping.class);
        }
        try {
            controlRuleMetricConfigMapping.getMetric(name).deleteRecord(key,amount);
        } catch (BizException e) {
            log.error("deleteRecord({},{},{})",name,key,amount,e);
        }
    }

    @Override
    public String getModuleName() {
        return "sip";
    }
}
