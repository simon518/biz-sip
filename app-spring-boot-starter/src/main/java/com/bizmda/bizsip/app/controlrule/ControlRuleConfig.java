package com.bizmda.bizsip.app.controlrule;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class ControlRuleConfig {
    private String preRatingScript;
    private String ratingScript;
    private String updatingScript;
    private List<ControlRule> controlRuleList;

    public ControlRuleConfig(Map<String,Object> controlRuleConfigmap) {
        this.preRatingScript = (String)controlRuleConfigmap.get("pre-rating-script");
        this.ratingScript = (String)controlRuleConfigmap.get("rating-script");
        this.updatingScript = (String)controlRuleConfigmap.get("updating-script");

        List<Map<String,Object>> mapList = (List<Map<String,Object>>)controlRuleConfigmap.get("rules");
        if (mapList == null) {
            mapList = new ArrayList<>();
        }
        this.controlRuleList = new ArrayList<>();
        for(Map<String,Object> map:mapList) {
            ControlRule controlRule = new ControlRule(map);
            this.controlRuleList.add(controlRule);
        }
    }
}
