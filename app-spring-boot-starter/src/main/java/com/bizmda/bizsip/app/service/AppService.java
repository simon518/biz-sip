package com.bizmda.bizsip.app.service;

import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.bizmda.bizsip.app.checkrule.*;
import com.bizmda.bizsip.app.config.AppServiceMapping;
import com.bizmda.bizsip.app.config.CheckRuleConfigMapping;
import com.bizmda.bizsip.app.config.ControlRuleConfigMapping;
import com.bizmda.bizsip.app.controlrule.ControlRule;
import com.bizmda.bizsip.app.controlrule.ControlRuleConfig;
import com.bizmda.bizsip.app.executor.AbstractAppExecutor;
import com.bizmda.bizsip.app.executor.script.MagicScriptHelper;
import com.bizmda.bizsip.common.*;
import com.bizmda.bizsip.service.AppLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ssssssss.script.MagicScriptContext;

import java.util.List;
import java.util.Map;

/**
 * @Author: 史正烨
 * @Date: 2022/3/1 4:04 下午
 * @Description:
 */
@Service
@Slf4j
public class AppService {
    @Autowired
    private AppServiceMapping appServiceMapping;
    @Autowired
    private CheckRuleConfigMapping checkRuleConfigMapping;
    @Autowired
    private ControlRuleConfigMapping controlRuleConfigMapping;
    @Autowired
    private AppLogService appLogService;

    public BizMessage<JSONObject> process(String appServiceId, JSONObject inJsonObject) {
        log.debug("开始处理App服务: {}", appServiceId);
        log.trace("App服务[{}]请求报文:\n{}", appServiceId, BizUtils.buildJsonLog(inJsonObject));

        BizMessage<JSONObject> bizMessage = BizMessage.createNewTransaction(appServiceId);
        bizMessage.setData(inJsonObject);
        BizMessage<JSONObject> inMessage = BizTools.copyBizMessage(bizMessage);
        BizTools.bizMessageThreadLocal.set(bizMessage);
        BizTools.tmContextThreadLocal.set(new TmContext());
        BizTools.serviceIdThreadLocal.set(appServiceId);

        BizMessage<JSONObject> outMessage = null;

        try {
            JSONArray jsonArray = this.checkFieldRule(appServiceId, inJsonObject);
            if (!jsonArray.isEmpty()) {
                throw new BizException(BizResultEnum.CHECKRULE_FIELD_CHECK_ERROR, jsonArray.toString());
            }

            jsonArray = this.checkServiceRule(appServiceId, inJsonObject);
            if (!jsonArray.isEmpty()) {
                throw new BizException(BizResultEnum.CHECKRULE_SERVICE_CHECK_ERROR, jsonArray.toString());
            }

            ControlRuleConfig controlRuleConfig = this.controlRuleConfigMapping.getControlRuleConfig(appServiceId);
            if (controlRuleConfig != null) {
                inJsonObject = this.doControlRule(controlRuleConfig, inJsonObject);
            }

            AbstractAppExecutor appExecutor = this.appServiceMapping.getAppExecutor(appServiceId);
            if (appExecutor == null) {
                log.error("App服务[{}]不存在", appServiceId);
                throw new BizException(BizResultEnum.APP_SERVICE_NOT_FOUND,
                        StrFormatter.format("App服务[{}]不存在", appServiceId));
            }


            log.debug("调用App服务[{}]: traceId={}", appServiceId, bizMessage.getTraceId());
            outMessage = appExecutor.doAppService(bizMessage);
            if (controlRuleConfig != null) {
                outMessage = this.updateControlRule(controlRuleConfig, inJsonObject, outMessage);
            }
        } catch (BizException e) {
            log.warn("App服务执行出错:{}-{}", e.getCode(),e.getMessage());
            outMessage = BizMessage.buildFailMessage(bizMessage, e);
        } finally {
            BizTools.tmContextThreadLocal.remove();
            BizTools.bizMessageThreadLocal.remove();
            BizTools.serviceIdThreadLocal.remove();
            BizTools.controlRuleThreadLocal.remove();
        }
        if (outMessage.getCode() == 0) {
            log.debug("发送交易成功日志");
            this.appLogService.sendAppSuccessLog(inMessage, outMessage);
        } else {
            log.debug("发送交易失败日志");
            this.appLogService.sendAppFailLog(inMessage, outMessage);
        }
        return outMessage;
    }

    private JSONArray checkFieldRule(String serviceId, JSONObject message) throws BizException {
        JSONArray jsonArray = new JSONArray();
        CheckRuleConfig checkRuleConfig = this.checkRuleConfigMapping.getCheckRuleConfig(serviceId);
        if (checkRuleConfig == null) {
            return jsonArray;
        }
        List<FieldCheckRule> fieldCheckRuleList = checkRuleConfig.getFieldCheckRuleList();
        if (fieldCheckRuleList == null) {
            return jsonArray;
        }
        log.debug("开始App服务[{}]域级规则校验", serviceId);

        List<FieldChcekRuleResult> fieldChcekRuleResultList = FieldCheckRuleHelper.checkFieldRule(message, fieldCheckRuleList, checkRuleConfig.getFieldCheckMode());

        for (FieldChcekRuleResult fieldChcekRuleResult : fieldChcekRuleResultList) {
            log.warn("域级校验规则不通过:{}-{}", fieldChcekRuleResult.getField(),
                    fieldChcekRuleResult.getMessage());
            JSONObject jsonObject = new JSONObject();
            jsonObject.set("field", fieldChcekRuleResult.getField());
            jsonObject.set("message", fieldChcekRuleResult.getMessage());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    private JSONArray checkServiceRule(String serviceId, JSONObject message) throws BizException {
        JSONArray jsonArray = new JSONArray();
        CheckRuleConfig checkRuleConfig = this.checkRuleConfigMapping.getCheckRuleConfig(serviceId);
        if (checkRuleConfig == null) {
            return jsonArray;
        }
        List<ServiceCheckRule> serviceCheckRuleList = checkRuleConfig.getServiceCheckRuleList();
        if (serviceCheckRuleList == null) {
            return jsonArray;
        }
        log.debug("开始App服务[{}]服务级规则校验", serviceId);

        List<ServiceChcekRuleResult> serviceChcekRuleResultList = ServiceCheckRuleHelper.checkServiceRule(message, serviceCheckRuleList, checkRuleConfig.getFieldCheckMode());

        for (ServiceChcekRuleResult serviceChcekRuleResult : serviceChcekRuleResultList) {
            log.warn("服务级校验规则不通过:{}", serviceChcekRuleResult.getResult());
            JSONObject jsonObject = new JSONObject();
            jsonObject.set("message", serviceChcekRuleResult.getResult());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    private JSONObject doControlRule(ControlRuleConfig controlRuleConfig, JSONObject inJsonObject) throws BizException {
        MagicScriptContext context = new MagicScriptContext();
        context.set("request", inJsonObject);
        if (!StrUtil.isBlank(controlRuleConfig.getPreRatingScript())) {
            log.debug("执行风控规则pre-rating-script脚本");
            MagicScriptHelper.executeScript(controlRuleConfig.getPreRatingScript(), context);
        }

        List<ControlRule> controlRuleList = controlRuleConfig.getControlRuleList();
        JSONArray jsonArray = new JSONArray();
        if (controlRuleList != null && controlRuleList.size() > 0) {
            log.debug("执行风控规则rules");
            List<ControlRuleResult> controlRuleResultList = ControlRuleHelper.controlRule(inJsonObject, controlRuleList);
            jsonArray = JSONUtil.parseArray(controlRuleResultList);
        }
        context.set("rules", jsonArray);
        JSONObject controlJsonObject = new JSONObject();
        context.set("control", controlJsonObject);

        if (!StrUtil.isBlank(controlRuleConfig.getRatingScript())) {
            log.debug("执行风控规则rating-script脚本");
            MagicScriptHelper.executeScript(controlRuleConfig.getRatingScript(), context);
        }
        Map<String, Object> map = context.getRootVariables();
        JSONObject jsonObject = (JSONObject) map.get("request");
        log.trace("rating-script脚本执行后request:\n{}", BizUtils.buildJsonLog(jsonObject));
        controlJsonObject = (JSONObject) map.get("control");
        String action = controlJsonObject.getStr("action");
        if ("error".equalsIgnoreCase(action)) {
            String message = controlJsonObject.getStr("message");
            log.warn("风控规则检查不通过:{}", message);
            throw new BizException(BizResultEnum.CONTROL_RULE_NO_PASS, message);
        }
        controlJsonObject.set("rules", jsonArray);
        log.trace("rating-script脚本执行后control:\n{}", BizUtils.buildJsonLog(controlJsonObject));
        BizTools.controlRuleThreadLocal.set(controlJsonObject);
        return jsonObject;
    }

    private BizMessage<JSONObject> updateControlRule(ControlRuleConfig controlRuleConfig, JSONObject inJsonObject, BizMessage<JSONObject> outBizMessage) throws BizException {
        if (StrUtil.isBlank(controlRuleConfig.getUpdatingScript())) {
            return outBizMessage;
        }
        JSONObject controlJsonObject = BizTools.controlRuleThreadLocal.get();
        boolean updatingFlag = controlJsonObject.getBool(BizConstant.CONTROL_RULE_UPDATING_FLAG, true);
        if (!updatingFlag) {
            return outBizMessage;
        }
        log.debug("执行风控规则updating-script脚本");

        MagicScriptContext context = new MagicScriptContext();
        context.set("request", inJsonObject);
        context.set("response", outBizMessage);
        context.set("control", controlJsonObject);
        MagicScriptHelper.executeScript(controlRuleConfig.getUpdatingScript(), context);
        Map<String, Object> map = context.getRootVariables();
        outBizMessage = (BizMessage<JSONObject>) map.get("response");
        log.trace("执行风控规则updating-script脚本后，返回response:\n{}", BizUtils.buildBizMessageLog(outBizMessage));
        return outBizMessage;
    }
}
