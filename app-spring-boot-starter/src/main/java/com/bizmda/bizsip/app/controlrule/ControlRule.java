package com.bizmda.bizsip.app.controlrule;

import lombok.Data;

import java.util.Map;

@Data
public class ControlRule {
    private String script;
    private String name;

    public ControlRule(Map<String,Object> map) {
        this.name = (String)map.get("name");
        String var = (String)map.get("script");
        if (var != null) {
            this.script = var;
            return;
        }
    }
}
