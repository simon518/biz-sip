package com.bizmda.bizsip.source.config;

import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.config.SourceConfigMapping;
import com.bizmda.log.trace.RestTemplateTraceInterceptor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.io.FileNotFoundException;
import java.util.Collections;

/**
 * @author 史正烨
 */
@Slf4j
@Configuration
@Getter
public class SourceConfiguration {
    @Value("${bizsip.config-path:#{null}}")
    private String configPath;
    @Value("${bizsip.integrator-url}")
    private String integratorUrl;
    @Bean
    public SourceConfigMapping sourceConfigMapping() throws BizException {
        return new SourceConfigMapping(this.configPath);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate()
    {
        RestTemplate restTemplate = new RestTemplate();
        log.info("RestTemplate调用增加日志跟踪能力拦截器");
        restTemplate.setInterceptors(Collections.singletonList(new RestTemplateTraceInterceptor()));
        return restTemplate;
    }
}
