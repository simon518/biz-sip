package com.bizmda.bizsip.config;

import cn.hutool.core.util.StrUtil;

import java.util.Map;

/**
 * @author 史正烨
 */
public class RestSinkConfig extends AbstractSinkConfig {
    private String url;

    public String getUrl() {
        return url;
    }

    public RestSinkConfig(Map map) {
        super(map);
        this.url = (String)map.get("url");
    }

    @Override
    public String toString() {
        String str = super.toString()
                + StrUtil.format(",url={}", this.url);
        return str;
    }
}
