package com.bizmda.log.monitor;

import lombok.extern.slf4j.Slf4j;

/**
 * 日志埋点工具类
 *
 * @author zlt
 */
@Slf4j
public class PointUtil {
    private static final String MSG_PATTERN = "{}|{}|{}";

    private PointUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static void info(String id, String type, String message) {
        log.info(MSG_PATTERN, id, type, message);
    }

    public static void debug(String id, String type, String message) {
        log.debug(MSG_PATTERN, id, type, message);
    }
}
