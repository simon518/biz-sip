package com.bizmda.bizsip.sample.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.bizmda.bizsip.sample.sink.api.AccountDTO;
import com.bizmda.bizsip.sample.sink.api.CustomerDTO;
import com.bizmda.bizsip.sample.sink.api.Sample16Interface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class Sample14AppService implements AppBeanInterface {
    private Sample16Interface sinkInterface1 = AppClientFactory
            .getSinkClient(Sample16Interface.class,"sample16-sink");
    private BizMessageInterface sinkInterface2 = AppClientFactory
            .getSinkClient(BizMessageInterface.class,"hello-sink");

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        String result1 = this.sinkInterface1.doService1("001");
        this.sinkInterface1.doService2("002",3);
        AccountDTO[] accountsDTO = this.sinkInterface1.queryAccounts(AccountDTO.builder().account("002").build());
        List<AccountDTO> accountListDTO = this.sinkInterface1.queryAccountList(AccountDTO.builder().account("002").build());
        CustomerDTO customerDTO = this.sinkInterface1.queryCustomerDTO("001");
        this.sinkInterface1.saveAllList(accountListDTO);
        this.sinkInterface1.saveAll(accountsDTO);
        this.sinkInterface1.testParamsType(accountListDTO,null,customerDTO);
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.set("accountNo","003");
        BizMessage<JSONObject> bizMessage = this.sinkInterface2.call(jsonObject1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("result1",result1);
        jsonObject.set("accountDTOs",accountsDTO);
        jsonObject.set("accountDTOList",accountListDTO);
        jsonObject.set("customerDTO",customerDTO);
        jsonObject.set("hello-sink",bizMessage.getData());
        return jsonObject;
    }
}
