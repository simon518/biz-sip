package com.bizmda.bizsip.sample.source.controller;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.bizmda.bizsip.common.BizConstant;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.bizmda.bizsip.source.api.SourceClientFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;

/**
 * @author 史正烨
 */
@Slf4j
@RestController
public class SampleSource1Controller {
    private BizMessageInterface appServiceClient = SourceClientFactory
            .getAppServiceClient(BizMessageInterface.class,"/bean/sample2");


    @PostMapping(value = "/source1", consumes = "application/json", produces = "application/json")
    public Object doService(@RequestBody String inMessage, HttpServletResponse response) throws BizException {
        JSONObject jsonObject = JSONUtil.parseObj(inMessage);
        return this.appServiceClient.call(jsonObject);
    }
}