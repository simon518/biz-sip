package com.bizmda.bizsip.sample.source.longtcpnetty;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * @author 史正烨
 */
public class ServerChannelInitializer extends ChannelInitializer<SocketChannel> {
    private SampleServerService serverService;

    public ServerChannelInitializer(SampleServerService serverService) {
        super();
        this.serverService = serverService;
    }
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        socketChannel.pipeline()
                .addLast(new IdleStateHandler(10, 0, 0, TimeUnit.SECONDS))
                .addLast(new SampleDecoder())
                .addLast(new NettyServerHandler(this.serverService));

        //添加编解码
//        socketChannel.pipeline().addLast("decoder", new StringDecoder(CharsetUtil.UTF_8));
//        socketChannel.pipeline().addLast("encoder", new StringEncoder(CharsetUtil.UTF_8));
//        socketChannel.pipeline().addLast(new NettyServerHandler(this.sourceService));
    }
}