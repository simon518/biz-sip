package com.bizmda.bizsip.sample.sink.api;

public interface HelloInterface {
    public String hello(String message);
}
