package com.bizmda.bizsip.sample.sink.listener;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.sample.sink.longtcpnetty.NettyClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

/**
 * RabbitMQ接收服务
 */
@Slf4j
@Service
public class LongTcpSinkQueueListener {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private NettyClient nettyClient;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "queue.bizsip.long-tcp-netty-sink", durable = "true", autoDelete = "false"),
            exchange = @Exchange(value = "exchange.direct.bizsip.sink", type = ExchangeTypes.DIRECT, durable = "true", autoDelete = "false"),
            key = "key.bizsip.long-tcp-netty-sink"))
    public void onMessage(Message inMessage) {
        try {
            send(inMessage);
        } catch (BizException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.set("code",e.getCode());
            jsonObject.set("message",e.getMessage());
            jsonObject.set("extMessage",e.getExtMessage());
            Message outMessage = MessageBuilder.withBody(jsonObject.toString()
                    .getBytes(StandardCharsets.UTF_8))
                    .setCorrelationId(inMessage.getMessageProperties().getCorrelationId())
                    .build();
            rabbitTemplate.send("",
                    inMessage.getMessageProperties().getReplyTo(),
                    outMessage);
            return;
        }
        catch (Exception e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.set("code",BizResultEnum.OTHER_ERROR);
            jsonObject.set("message",e.getMessage());
            Message outMessage = MessageBuilder.withBody(jsonObject.toString()
                            .getBytes(StandardCharsets.UTF_8))
                    .setCorrelationId(inMessage.getMessageProperties().getCorrelationId())
                    .build();
            rabbitTemplate.send("",
                    inMessage.getMessageProperties().getReplyTo(),
                    outMessage);
            return;
        }
        return;
    }

    private void send(Message inMessage) throws BizException {
        String correlationId = inMessage.getMessageProperties().getCorrelationId();
        String replyTo = inMessage.getMessageProperties().getReplyTo();
        byte[] bytes = inMessage.getBody();
        log.trace("correlationId,replayTo: {},{}",correlationId,replyTo);
        log.trace("message.getBody():\n{}",BizUtils.buildHexLog(bytes));

        try {
            String str = new String(bytes,"UTF-8");
            JSONObject jsonObject = JSONUtil.parseObj(str);
            jsonObject.set("correlationId",correlationId);
            jsonObject.set("replyTo",replyTo);
            bytes = jsonObject.toString().getBytes(StandardCharsets.UTF_8);
        } catch (UnsupportedEncodingException e) {
            throw new BizException(BizResultEnum.OTHER_ERROR,e);
        }

        this.nettyClient.sendData(bytes);

//        Message outMessage = MessageBuilder.withBody(bytes).setCorrelationId(correlationId).build();
//        rabbitTemplate.send("", replyTo, outMessage);
    }
}
