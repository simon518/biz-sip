package com.bizmda.bizsip.sink.config;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.config.RabbitmqSinkConfig;
import com.bizmda.bizsip.config.RestSinkConfig;
import com.bizmda.bizsip.config.SinkConfigMapping;
import com.bizmda.bizsip.sink.processor.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 史正烨
 */
@Slf4j
@Configuration
public class SinkConfiguration {
    @Value("${bizsip.config-path:#{null}}")
    private String configPath;
    @Value("${bizsip.sink-id:#{null}}")
    private String sinkId;
    @Value("${bizsip.rabbitmq-log:#{null}}")
    private String rabbitmqLog;

    @Autowired
    private CachingConnectionFactory connectionFactory;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;
    private Map<String, DirectExchange> sinkExchangeMap;

    @Bean
    public SinkConfigMapping sinkConfigMapping() {
        try {
            SinkConfigMapping sinkConfigMapping = new SinkConfigMapping(this.configPath);
            return sinkConfigMapping;
        } catch (BizException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Bean
    @ConditionalOnProperty(name = "spring.rabbitmq.host", matchIfMissing = false)
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Map<String, DirectExchange> sinkExchangeMap(SinkConfigMapping sinkConfigMapping) throws BizException {
        String[] sinkIds;
        if (StrUtil.isEmpty(this.sinkId)) {
            sinkIds = new String[0];
        } else {
            sinkIds = this.sinkId.split(",");
        }
        this.sinkExchangeMap = new HashMap<String, DirectExchange>();
        for (String sinkId : sinkIds) {
            AbstractSinkConfig sinkConfig = sinkConfigMapping.getSinkConfigMap().get(sinkId);
            if (sinkConfig == null) {
                log.error("Sink[{}]没有在sink.yml中定义，装载失败!", sinkId);
                continue;
            }
            log.info("初始化Sink服务[{}]: {}", sinkId,sinkConfig.toString());
            AbstractSinkProcessor sinkProcessor;

            switch (sinkConfig.getProcessor()) {
                case AbstractSinkConfig.PROCESSOR_DEFAULT:
                    log.info("Sink服务[{}],初始化Processor[default]", sinkId);
                    sinkProcessor = new SinkProcessor(sinkConfig);
                    break;
                case AbstractSinkConfig.PROCESSOR_BEAN:
                    log.info("Sink服务[{}],初始化Processor[bean]", sinkId);
                    sinkProcessor = new BeanSinkProcessor(sinkConfig);
                    break;
                case AbstractSinkConfig.PROCESSOR_SINK_BEAN:
                    log.info("Sink服务[{}],初始化Processor[sink-bean]", sinkId);
                    sinkProcessor = new SinkBeanSinkProcessor(sinkConfig);
                    break;
                default:
                    continue;
            }

            switch (sinkConfig.getType()) {
                case AbstractSinkConfig.TYPE_REST:
                    log.info("Sink服务[{}],初始化同步调用接口(RestController)", sinkId);
                    this.regiestSinkController(sinkConfig, sinkProcessor);
                    break;
                case AbstractSinkConfig.TYPE_RABBITMQ:
                    log.info("Sink服务[{}],初始化异步调用接口(RabbitmqListener)", sinkId);
                    this.regiestSinkMessageListener(sinkConfig, sinkProcessor);
                    break;
                default:
                    continue;
            }

        }
        return this.sinkExchangeMap;
    }


    private void regiestSinkController(AbstractSinkConfig sinkConfig, AbstractSinkProcessor sinkProcessor) throws BizException {
        if (!(sinkConfig instanceof RestSinkConfig)) {
            return;
        }
        RestSinkConfig restSinkConfig = (RestSinkConfig) sinkConfig;
        String path = URLUtil.getPath(restSinkConfig.getUrl());
        RequestMappingInfo info = RequestMappingInfo.paths(path).consumes("application/json").produces("application/json").methods(RequestMethod.POST).build();
        Method method = null;
        try {
            method = SinkRestController.class.getMethod("service", BizMessage.class);
        } catch (NoSuchMethodException e) {
            log.error("SinkRestController获取方法出错!", e);
            throw new BizException(BizResultEnum.OTHER_ERROR, e);
        }
        SinkRestController sinkRestController = new SinkRestController(sinkConfig);
        requestMappingHandlerMapping.registerMapping(info, sinkRestController, method);
        log.info("注册Sink服务[{}]同步接口: path={}", sinkConfig.getId(), path);
    }


    private void regiestSinkMessageListener(AbstractSinkConfig sinkConfig, AbstractSinkProcessor sinkProcessor) throws BizException {
        ConfigurableApplicationContext context = (ConfigurableApplicationContext) applicationContext;
        RabbitmqSinkConfig rabbitmqSinkConfig = (RabbitmqSinkConfig) sinkConfig;
        SinkMessageListener sinkMessageListener = new SinkMessageListener(sinkConfig, rabbitTemplate(), this.rabbitmqLog);
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        Environment env = applicationContext.getEnvironment();
        container.setConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.concurrency", int.class));
        container.setMaxConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.max-concurrency", int.class));
        container.setPrefetchCount(env.getProperty("spring.rabbitmq.listener.simple.prefetch", int.class));
        Queue queue = new Queue(rabbitmqSinkConfig.getQueue(), true, false, false);
        context.getBeanFactory().registerSingleton("rabbitmq-queue-" + rabbitmqSinkConfig.getId(), queue);
        DirectExchange directExchange = this.sinkExchangeMap.get(rabbitmqSinkConfig.getExchange());
        if (directExchange == null) {
            directExchange = new DirectExchange(rabbitmqSinkConfig.getExchange(), true, false);
            context.getBeanFactory().registerSingleton("rabbitmq-exchange-" + rabbitmqSinkConfig.getExchange(), directExchange);
            this.sinkExchangeMap.put(rabbitmqSinkConfig.getExchange(), directExchange);
        }
        Binding binding = BindingBuilder.bind(queue).to(directExchange).with(rabbitmqSinkConfig.getRoutingKey());
        context.getBeanFactory().registerSingleton("rabbitmq-binding-" + rabbitmqSinkConfig.getId(), binding);
        container.setQueues(queue);
        container.setAcknowledgeMode(AcknowledgeMode.AUTO);
        container.setMessageListener(sinkMessageListener);
        context.getBeanFactory().registerSingleton("rabbitmq-listener-" + rabbitmqSinkConfig.getId(), container);

        log.info("注册Sink服务[{}]异步接口: queue={},exchange={},key={}",
                sinkConfig.getId(), rabbitmqSinkConfig.getQueue(),
                rabbitmqSinkConfig.getExchange(), rabbitmqSinkConfig.getRoutingKey());
        return;
    }
}
