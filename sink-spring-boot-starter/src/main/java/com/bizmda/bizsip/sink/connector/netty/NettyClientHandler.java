package com.bizmda.bizsip.sink.connector.netty;

import com.bizmda.bizsip.common.BizConstant;
import com.bizmda.bizsip.common.BizUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 史正烨
 */
@Slf4j
public class NettyClientHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.trace("通道激活...");
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        byte[] value;
        if (msg instanceof ByteBuf) {
            value = ((ByteBuf) msg).array();
        }
        else {
            value = msg.toString().getBytes(BizConstant.DEFAULT_CHARSET_NAME);
        }
        log.trace("Channel通道收到报文:\n{}", BizUtils.buildHexLog(value));

        AttributeKey<byte[]> key = AttributeKey.valueOf("ServerData");
        ctx.channel().attr(key).set(value);

        //把客户端的通道关闭
        ctx.channel().close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
